import 'intersection-observer'

const works = document.querySelectorAll('.works__list')
const ovserver = new IntersectionObserver(
  entries => {
    entries.forEach(({ isIntersecting, target }) => {
      target.classList[isIntersecting ? 'add' : 'remove']('is-active')
    })
  },
  {
    rootMargin: '0px 0px -30% 0px',
  },
)
;[...works].forEach(element => {
  ovserver.observe(element)
})

// const doc = window.document
// const context = doc.querySelector('.works')
// const clones = context.querySelectorAll('.is-clone')
// let disableScroll = false
// let scrollHeight = 0
// let scrollPos = 0
// let clonesHeight = 0
// let i = 0

// function getScrollPos() {
//   return (context.pageYOffset || context.scrollTop) - (context.clientTop || 0)
// }

// function setScrollPos(pos) {
//   context.scrollTop = pos
// }

// function getClonesHeight() {
//   clonesHeight = 0

//   for (i = 0; i < clones.length; i += 1) {
//     clonesHeight = clonesHeight + clones[i].offsetHeight
//   }

//   return clonesHeight
// }

// function reCalc() {
//   scrollPos = getScrollPos()
//   scrollHeight = context.scrollHeight
//   clonesHeight = getClonesHeight()

//   if (scrollPos <= 0) {
//     setScrollPos(1)
//   }
// }

// function scrollUpdate() {
//   if (!disableScroll) {
//     scrollPos = getScrollPos()

//     if (clonesHeight + scrollPos >= scrollHeight) {
//       setScrollPos(1)
//       disableScroll = true
//     } else if (scrollPos <= 0) {
//       setScrollPos(scrollHeight - clonesHeight)
//       disableScroll = true
//     }
//   }

//   if (disableScroll) {
//     window.setTimeout(function() {
//       disableScroll = false
//     }, 40)
//   }
// }

// function init() {
//   reCalc()

//   context.addEventListener(
//     'scroll',
//     function() {
//       window.requestAnimationFrame(scrollUpdate)
//     },
//     false,
//   )

//   window.addEventListener(
//     'resize',
//     function() {
//       window.requestAnimationFrame(reCalc)
//     },
//     false,
//   )
// }

// if (document.readyState !== 'loading') {
//   init()
// } else {
//   doc.addEventListener('DOMContentLoaded', init, false)
// }

// window.onload = function() {
//   setScrollPos(
//     Math.round(
//       clones[0].getBoundingClientRect().top +
//         getScrollPos() -
//         (context.offsetHeight - clones[0].offsetHeight) / 2,
//     ),
//   )
// }
